﻿/*
 * Created by SharpDevelop.
 * User: putzolu
 * Date: 28/02/2017
 * Time: 12:21
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Reflection;
using System.Data.SQLite; 
using System.Collections;
using System.Collections.Generic;

namespace Togu.Models
{
	/// <summary>
	/// Description of SQLiteAdapter.
	/// </summary>
	public class SQLiteAdapter : AdapterBase
	{	
		private Hashtable typeDictionary = new Hashtable();	
		private string dataFileFullPath;
		public string DataFileFullPath{
			get{
				return dataFileFullPath;
			}
			set{
				dataFileFullPath = value;
				this.ConnectionString = "Data Source=" + dataFileFullPath + ";Version=3;";
			}
		}
		
		public string ConnectionString{
			get;
			set;
		}
		
		public void CreateDataFile(){
			//check if file exist
			if(Directory.Exists(Path.GetDirectoryName(this.DataFileFullPath)))
			{
				if(!File.Exists(this.DataFileFullPath))
				{
					SQLiteConnection.CreateFile(this.DataFileFullPath);
				}
			}
			else
			{
				throw new DirectoryNotFoundException("[Directory Not Found]");
			}
		}
		
			
		private void createTable(SQLiteCommand com, string tableName, PropertyInfo[] fields)
		{
			string sqlCheck = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "';";
						
			string sql = "create table " + tableName + " (id INTEGER PRIMARY KEY,";
			foreach (PropertyInfo pi in fields){	
				if(pi.Name!="Id"){
					sql += pi.Name + " " + typeDictionary[pi.PropertyType.Name] + ", ";
				}
			}

			sql = cutFinalComma(sql);			
			sql +=")";			

			com.CommandText = sqlCheck;
			var ret = com.ExecuteScalar();
			//creates table if not already exists
			if(null == ret){
    			com.CommandText = sql;    
    			com.ExecuteNonQuery();  
    			Console.WriteLine(sql);	
			}					
		}
				
		
		private int insert(SQLiteCommand com, System.Object obj, string tableName, PropertyInfo[] fields)
		{			
			string sql = "INSERT INTO " + tableName +"(";			
			foreach (PropertyInfo pi in fields)
			{	if(pi.Name!="Id")
				{
					sql += pi.Name + ", ";
				}
			}
			
			sql = cutFinalComma(sql);			
			sql += ") Values (";
			
			string stringDelimiter;
			foreach (PropertyInfo pi in fields)
			{	if(pi.Name!="Id")
				{	stringDelimiter = "";
					if((typeDictionary[pi.PropertyType.Name]).ToString()=="TEXT")
					{
						stringDelimiter = "'";
					}
					sql += stringDelimiter + pi.GetValue(obj, null) + stringDelimiter + ", ";
				}
			}
			
			sql = cutFinalComma(sql);
			
			sql += ");";
			                  
			com.CommandText = sql;    
			com.ExecuteNonQuery(); 
			Console.WriteLine(sql);  

			sql = "SELECT last_insert_rowid();";
			com.CommandText = sql;
			
			Object o = com.ExecuteScalar();
			return Convert.ToInt32(o);
		}
				
		public override int Save(System.Object obj, string tableName, PropertyInfo[] fields)
		{
			int lastInserted = 0;
			//use single connection
			using (SQLiteConnection con = new System.Data.SQLite.SQLiteConnection(this.ConnectionString))
			{
				con.Open(); 
				using(SQLiteTransaction tr = con.BeginTransaction())
            	{
	    			using (SQLiteCommand com = new SQLiteCommand(con))
	    			{			
	    				com.Transaction = tr;
						createTable(com, tableName,fields); 
						lastInserted = insert(com, obj, tableName,fields); //todo: if Id is null -> INSERT else UPDATE						
						com.Dispose();					
	    			}
	    			tr.Commit();
				}
				con.Close(); 
			}
			return lastInserted;
		}
			
		public override T Find_by_id<T>(int id2Find){
			Type myType = typeof(T);
			string tableName = getTableName(myType);
			
			var instance = Activator.CreateInstance(myType);
			string sql =  "SELECT * FROM " + tableName + " WHERE Id=" + id2Find + ";";
			
			using (SQLiteConnection con = new System.Data.SQLite.SQLiteConnection(this.ConnectionString))
			{
    			using (SQLiteCommand com = new SQLiteCommand(con))
    			{			
					con.Open();    
    				com.CommandText = sql;    
    				SQLiteDataReader sqReader = com.ExecuteReader();  
    				sqReader.Read();
    				if (sqReader.HasRows){
    					MethodInfo getPropertiesMethod = instance.GetType().GetMethod("GetProperties");
	                	PropertyInfo[]  props =  (PropertyInfo[])getPropertiesMethod.Invoke(instance, new object[0]);
	                	//populates instance with query result inferring types from instance properties	
	                	foreach (PropertyInfo pi in props){               		
	                		instance.GetType().GetProperty(pi.Name).SetValue(instance,Convert.ChangeType(sqReader[pi.Name],pi.PropertyType),null);
	                	}
    				}
    				else{
    					instance = null;
    				}
    				com.Dispose();
    			}
    			con.Close();
			}			
			return (T)instance;
		}
		
		public override void Delete_by_id<T>(int id2Delete){
			//SQLiteConnection.ClearAllPools();
			Type myType = typeof(T);
			string tableName = getTableName(myType);
			string sql = "DELETE FROM " + tableName +" WHERE Id = " + id2Delete + " ;";
			using (SQLiteConnection con = new System.Data.SQLite.SQLiteConnection(this.ConnectionString))
			{
    			using (SQLiteCommand com = new SQLiteCommand(con))
    			{			
					con.Open();                             
        			com.CommandText = sql;  
        			com.ExecuteNonQuery();
        			Console.WriteLine(sql);
        			com.Dispose();
    			}
    			con.Close();
			}        			
	    }
		
		public override void Update_by_id<T>(int id2Update, Hashtable ht){
			Type myType = typeof(T);
			string tableName = getTableName(myType);
			string sql = "UPDATE " + tableName +" SET ";
			
			string strDelimiter = "";
			foreach (DictionaryEntry entry in ht)
	        {
				if(entry.Value is string){
					strDelimiter = "'";
				}
				else{
					strDelimiter = "";
				}
				sql += entry.Key + " = " + strDelimiter + entry.Value + strDelimiter + " , ";
				
				Console.WriteLine("{0}, {1}", entry.Key, entry.Value);
	        }
			
			sql = cutFinalComma(sql);
			sql += " WHERE Id = " +  id2Update + ";";
					
			using (SQLiteConnection con = new System.Data.SQLite.SQLiteConnection(this.ConnectionString))
			{
    			using (SQLiteCommand com = new SQLiteCommand(con))
    			{			
					con.Open();                             
        			com.CommandText = sql;  
        			com.ExecuteNonQuery();
        			Console.WriteLine(sql);
        			com.Dispose();
    			}
    			con.Close();
			}  
 		}
					
		public override List<T> All<T>()
		{
			//todo:    change to  public override List<T> All<T>()
			Type myType = typeof(T);
			string tableName = getTableName(myType);
			var ret = new List<T>();
			
			string sql = "SELECT * FROM " + tableName + ";";
			using (SQLiteConnection con = new System.Data.SQLite.SQLiteConnection(this.ConnectionString))
			{
    			using (SQLiteCommand com = new SQLiteCommand(con))
    			{			
					con.Open();                             
        			com.CommandText = sql;    
        			SQLiteDataReader sqReader = com.ExecuteReader();  

	                while (sqReader.Read()) 
	                {	                	
	                	//creates new instance of myType
	                	var instance = Activator.CreateInstance(myType);
	                	MethodInfo getPropertiesMethod = instance.GetType().GetMethod("GetProperties");
	                	PropertyInfo[]  props =  (PropertyInfo[])getPropertiesMethod.Invoke(instance, new object[0]);
	                	//populates instance with query result inferring types from instance properties	
	                	foreach (PropertyInfo pi in props){	                		
	                		instance.GetType().GetProperty(pi.Name).SetValue(instance,Convert.ChangeType(sqReader[pi.Name],pi.PropertyType),null);
	                	}
	                	//add instances to ret
	                	ret.Add((T)instance);
	                }
    			}
    			con.Close();
			}
			return ret;
		}

		public override T Create<T>(Hashtable ht){
			Console.WriteLine("Create method called");
			Type myType = typeof(T);	
			var instance = (T)Activator.CreateInstance(myType);			
			foreach (DictionaryEntry entry in ht)
			{
				instance.GetType().GetProperty(entry.Key.ToString()).SetValue(instance, entry.Value);
			}
			
			instance.GetType().GetMethod("Save").Invoke(instance, new object[0]);
			return instance;
		}
		
		public SQLiteAdapter()
		{	
			//C# to Sqlite
	        this.typeDictionary["String"] = "TEXT";
	        this.typeDictionary["Int32"] = "INTEGER";
	        //Sqlite to C#
	       	this.typeDictionary["TEXT"] = "String";
	        this.typeDictionary["INTEGER"] = "Int32";
	        //todo: initTypes method. Check SQLite limits
		}
		
		
		
	}
	
	
	
}
