﻿/*
 * Created by SharpDevelop.
 * User: putzolu
 * Date: 28/02/2017
 * Time: 17:52
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace Togu.Models{
	/// <summary>
	/// Description of AdapterBase.
	/// </summary>
	public abstract class AdapterBase{
		protected string cutFinalComma(string myStr)
		{
			if(myStr.EndsWith(", ",StringComparison.Ordinal))
			{
				myStr = myStr.Substring(0,myStr.Length - 2);
			}
			return myStr;
		}
		
		protected string getTableName(Type myType){
			return myType.Name.ToLower() + "s";
		}
		
		public abstract int Save(Object obj, string tableName, PropertyInfo[] fields);
		public abstract  List<T> All<T>();	
		public abstract T Find_by_id<T>(int id2Find); 
		public abstract void Delete_by_id<T>(int id2Delete);
		public abstract void Update_by_id<T>(int id2Update, Hashtable ht);
		public abstract T Create<T>(Hashtable ht); 
		
	}
}
