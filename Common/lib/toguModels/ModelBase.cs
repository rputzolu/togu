using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

 namespace Togu.Models
 {
 	public class ModelBase
	{
		//public static AdapterBase Adapter;
		
		private static Hashtable adapterTable = new Hashtable();
		public static void SetAdapter<T>(AdapterBase adapt){
			adapterTable[(typeof(T)).Name] = adapt;
		}
		
		public void  Save()
		{
			Console.WriteLine("Caller type: {0} Method: Save()", this.GetType().Name);
			
			AdapterBase Adapter = (AdapterBase)adapterTable[this.GetType().Name];
			
			int lastInserted = Adapter.Save(this, this.GetType().Name.ToLower() + "s", this.GetProperties()); //todo: move table name and properties in adapter
			this.Id = lastInserted;
		}
				
		public static List<T> All<T>() where T : ModelBase
		{
			Console.WriteLine("Caller type: {0} Method: All()", (typeof(T)).Name);
			AdapterBase Adapter = (AdapterBase)adapterTable[(typeof(T)).Name];
			return Adapter.All<T>();
		}
			
		public static T Find_by_id<T>(int id2Find) where T : ModelBase{	
			Console.WriteLine("Caller type: {0} Method: Find_by_id()", (typeof(T)).Name);			
			AdapterBase Adapter = (AdapterBase)adapterTable[(typeof(T)).Name];
			return Adapter.Find_by_id<T>(id2Find);							
		}
		
		public static void Delete_by_id<T>(int id2Delete) where T : ModelBase{
			Console.WriteLine("Caller type: {0} Method: Delete_by_id()", (typeof(T)).Name);	
			AdapterBase Adapter = (AdapterBase)adapterTable[(typeof(T)).Name];			
			Adapter.Delete_by_id<T>(id2Delete);							
		}
		

		public static void Update_by_id<T>(int id2Update, Hashtable  ht) where T : ModelBase{	
			Console.WriteLine("Caller type: {0} Method: Update_by_id()", (typeof(T)).Name);	
			AdapterBase Adapter = (AdapterBase)adapterTable[(typeof(T)).Name];			
			Adapter.Update_by_id<T>(id2Update,ht);							
		}
		
		//constructor
		public ModelBase(){}
			
		public static T New<T>()
		{
			var mb  = Activator.CreateInstance(typeof(T));
			return (T)mb;
		}
		
		public static T Create<T>(Hashtable ht){
			AdapterBase Adapter = (AdapterBase)adapterTable[(typeof(T)).Name];
			return Adapter.Create<T>(ht);
		}
		
		public PropertyInfo[] GetProperties()
		{
			PropertyInfo[] propertyInfos;
			propertyInfos = this.GetType().GetProperties();
			return propertyInfos;
		}
						
		//primary key 
		public int Id {
			get;
			set;
		}
	}
 }