function toHTML{
	param(
		$block = {}	
	)		
	& $block		
}

# function toHTML{
	# param(
		# $block = {},
		# $sbParams
	# )		
	# write-host 'inside toHTML ' $sbParams	
	# write-host  'inside toHTML '  $block
	# &  $block $sbParams 	
# }

function replaceText{
	param(
		$text,
		$replacementlist
	)
	Invoke-Expression (
		'$text' + -join $(
			foreach($e in $replacementlist.Split('#')) { 
				'.Replace("{0}","{1}")' -f $e, $(
				[void]$foreach.MoveNext()
				$foreach.Current) 
			} 
		)
	)
}

function topHtml{
	param(
		$pageTitle = 'page title',
		$useJQuery = $true,
		$useBootstrapCss = $true
	)
	
	if($useJQuery){
		$JQ = '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>'
	}
	
	if($useBootstrapCss){
		$BS = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">'
		$BS += '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>'
	}
	
	'<!DOCTYPE html>
	<html>
	<head>
	<title>'+$pageTitle+'</title> ' + $JQ + $BS + '</head>	
	<body>'
}

function bottomHtml{
	'</body>
	</html>
	'
}

# function renderView{
	# param($myView)
	# $ret = '404'
	#todo check if file /views/showUser exists
	# $fileName = '.\views\'+$myView+'.togu'
	# write-host $fileName
	# if(Test-Path $fileName){
		# $content = Get-Content $fileName | Out-String  #read file as text
			# $content = "'" + $content + "'"
			# $replacementlist = "<%#';#%>#;'"
			# $content = replaceText $content $replacementlist
			# $scriptBlock = [Scriptblock]::Create($content)  
			# $ret = toHTML $scriptBlock
	# }
	# return $ret	
# }

function renderView{
	param($myView, $myParams)
	
	$ret = '404'
	$fileName = '.\views\'+$myView+'.togu'

	if(Test-Path $fileName){
		$content = Get-Content $fileName | Out-String  #read file as text
			$content = "'" + $content + "'"
			##todo: check if can inject this in view before preprocessing: <script>var uid = <%$pars%>;</script>
			$replacementlist = "<%#';#%>#;'"
			$content = replaceText $content $replacementlist
			$content = 'param($viewParams);' + $content 
			$scriptBlock = [Scriptblock]::Create($content)  
			&  $scriptBlock $myParams  
	}
	else{
		return $ret	
	}
}


