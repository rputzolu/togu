function manageRestReq{
	param(
		$req
	)		
	
	$routingParams = @()
	$urlPieces = $req.Url.LocalPath.split('/')
	$path2Find = '.'
	foreach($up in $urlPieces){
		$parent = $path2Find
		$path2Find += $up + '/'		
		$found = test-path $path2Find 
		#write-host $path2Find $found
		if(-not $found){
			write-host 'searching' $parent'_id/'
			$pathParam = test-path $parent'_id/'
			if(-not $pathParam){
				return '404'
			}
			else{
				$routingParams += $up
				write-host 'parameter values: ' $routingParams				
				$path2Find = $parent + '_id/'
			}
		}
	}
	
	$handler = $path2Find + $req.HttpMethod + '.ps1'
		if(test-path $handler){		
			. $handler			
			$actionName = $req.HttpMethod.ToLower()
			$sb = (get-command $actionName  -CommandType Function).ScriptBlock
			#$res = invoke-command -scriptblock $sb -ArgumentList $routingParams	
			#write-host 'qstr '$req.QueryString
			#write-host 'req '($req.toString())
			$req | Format-List -Property * > c:\temp\pippo.txt
			
			$reqParams = @{}
			foreach($k in $req.QueryString){
				$reqParams[$k] = $req.QueryString[$k]
			} 
			
			#retrieve form fields todo: limit to post/put ?
			$sr = New-Object System.IO.StreamReader($req.InputStream,$req.ContentEncoding)
			$dataText = $sr.ReadToEnd()				
			$nvcoll =  [System.Web.HttpUtility]::ParseQueryString($dataText);
			$formFields = @{}
			foreach($k in $nvcoll){
				#Write-Host 'Form field in routing.ps1 : ' $k ' : ' $nvcoll[$k]
				$formFields[$k]=$nvcoll[$k]
			}
				
			
			#$res = invoke-command -scriptblock $sb -ArgumentList @{"routingParams" = $routingParams; "reqParams"=$reqParams }
			$res = invoke-command -scriptblock $sb -ArgumentList @{"pathParams" = $routingParams; "queryString"=$reqParams; "formFields"=$formFields}
			
			
			
			return $res
		}		
	return '404'
}
