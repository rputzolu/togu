add-type -assembly system.web.extensions

function toJson([object] $item){    
    $ps_js=new-object system.web.script.serialization.javascriptSerializer
    return $ps_js.Serialize($item)
}

function fromJson([object] $item){     
    $ps_js=new-object system.web.script.serialization.javascriptSerializer
    #The comma operator is the array construction operator in PowerShell
    #return ,$ps_js.DeserializeObject($item)
    return $ps_js.DeserializeObject($item)
}



# function fromjson {
	# param(
		# [parameter(mandatory=$true)]		
		# $text
		# )
	# $text = '$z ='+ $text
	# if($text -match '(\".:)\\'){
	# #if($text -match '(".*)(:)(?*")'){
		# $text = $text.replace($matches[0], $matches[0].replace(":","�")) #todo: replace all occcurences
	# }

	# $replacementlist = '{#@{#[#@(#]#)#:#=#,#;#�#:'
	# $res = replacetext $text $replacementlist
	# invoke-expression $res
	# return $z
# }



# function toJson {
	# param(
		# [Parameter(Mandatory=$true)]
		# $obj
		# )
		# $res = ""
	# switch ($obj.GetType().Name) { 
        # "Object[]" {
			# $res += expandArray $obj 'j'
		# }
		# "Hashtable" {
			# $res += expandHash $obj 'j'
		# }
	# }
	# return $res
# }

# function expandArray {
	# param(
		# $obj,
		# $jtype
	# )
	
	# if($jtype -eq 'p'){
		# $start = '@('
		# $end = ')'
		# $separator = ','		
	# }
	# else{
		# $start = '['
		# $end = ']'
		# $separator = ','	
	# }
	# $a = $start
	# foreach($e in $obj){
		# switch ($e.GetType().Name){
			# "String" {$a +=  [System.String]::Concat('"',$e,'"',$separator)}
			# "Hashtable" {$a +=  [System.String]::Concat((expandHash $e $jtype),$separator) }
			# "Object[]" {$a +=  [System.String]::Concat((expandArray $e $jtype),$separator)} 
			# default  {$a +=   [System.String]::Concat($e,$separator)}		
		# }	
	# }
	# if($a.EndsWith($separator)){
		# $a = $a.Substring(0,($a.Length)-1)
	# }
	# $a += $end
	# return $a	
# }

# function expandHash {
	# param(
		# $obj,
		# $jtype
	# )
	# if($jtype -eq 'p'){
		# $start = '@{'
		# $end = '}'
		# $separator = ';'
		# $assignement = '='
	# }
	# else{
		# $start = '{'
		# $end = '}'
		# $separator = ','
		# $assignement = ':'
	# }
		
	# $h = $start
	# foreach($k in $obj.Keys){
		# switch ($k.GetType().Name){
			# "String" {
				# $h+= [System.String]::Concat('"',$k,'"',$assignement) 
				# }
			# default {
				# $h +=  [System.String]::Concat($k,$assignement)
			# }
		# }
		# switch ($obj[$k].GetType().Name){
			# "String" {$h +=[System.String]::Concat('"',$obj[$k],'"',$separator)}
			# "Hashtable" {$h +=  [System.String]::Concat((expandHash $obj[$k] $jtype),$separator)}
			# "Object[]" {$h +=  [System.String]::Concat((expandArray $obj[$k] $jtype),$separator)} 
			# default  {$h +=  [System.String]::Concat($obj[$k], $separator)}
		# }					
	# }
	# if($h.EndsWith($separator)){
		# $h = $h.Substring(0,($h.Length)-1)
	# }
	# $h += $end
	# return $h
# }
# function toPson {
	# param(
		# [Parameter(Mandatory=$true)]
		# $obj
		# )
		# $res = ""
		
	# switch ($obj.GetType().Name) { 
        # "Object[]" {
			# $res += expandArray $obj 'p'
		# }
		# "Hashtable" {
			# $res += expandHash $obj 'p'
		# }
	# }
	# return $res
# }


<#TEST
cls
$test = fromJson('{"PATH":"c:\\pippo\\pluto.txt","aaa":"f:\\ciao"}')
$test

toJson($test)
#>