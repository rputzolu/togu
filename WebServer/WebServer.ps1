#import external functions
. ..\Common\lib\jsonSerialization.ps1
. ..\Common\lib\utils.ps1

#read configuration
[xml]$xmlConfig =  Get-Content ../WebServer/Config.xml

#get static path from $args[0] if passed
if($args[0] -ne $null){
	$staticPath = $args[0]
}
else {
	$staticPath = "..\Common\web\"
}

#execute custom app's initialize script
$appInitFile = $staticPath + '\..\lib\initialize.ps1' 
if(test-path $appInitFile){
	. $appInitFile
}

$enc = [system.Text.Encoding]::UTF8
$routes = @{
	"/ping" = {
		return $enc.GetBytes("pong") 		
		};
    "/hello" = { return $enc.GetBytes('<html><body>Ajo'' world!</body></html>')};
	"/" =  { return $enc.GetBytes('<html><body>root</body></html>')};
	"/call" =  { 
			$action = $context.Request.QueryString["action"]
			$argList = $context.Request.QueryString["data"]	
			$lib = [System.IO.Path]::Combine($staticPath, [System.String]::Concat('..\lib\', $action ,'.ps1'))
			. $lib 			
			$sb = (get-command $action  -CommandType Function).ScriptBlock
			$res = invoke-command -scriptblock $sb -ArgumentList $argList	
			return $enc.GetBytes($res) 
	 };
	 "/getverb" = {
		return $enc.GetBytes('<html><body>method: ' + $context.Request.HttpMethod + '</body></html>')
	 };
	"/api"={
		. ..\Common\lib\routing.ps1
		$ret = manageRestReq($context.Request)					
		return $ENC.GetBytes($ret)
	}	 
}

$url = $xmlConfig.config.server_url.ToString()
if(-not $url.EndsWith('/')){
	$url+='/'
}

$listener = New-Object System.Net.HttpListener
$listener.Prefixes.Add($url)
$listener.Start()
Write-Host "Listening at $url..."

#launch the browser if argument 1 has been passed
if($args[1] -ne $null){ 
	& $xmlConfig.config.browser_exe_fullpath @($args[1])
}

while ($listener.IsListening)
{
    $context = $listener.GetContext()
    $requestUrl = $context.Request.Url
    $response = $context.Response

    Write-Host "url > $requestUrl"
	
	###KO
	# write-host 'num fields ' $context.RequestRequest.Form.Count
	# foreach($k in $context.Request){
		# write-host 'key '$k 
	# }  
	###KO
	
	##OK todo: move in routing.ps1
	# $sr = New-Object System.IO.StreamReader($context.Request.InputStream,$context.Request.ContentEncoding)
	# $dataText = $sr.ReadToEnd()			
	##Write-Host 'input stream ' $dataText
	##Write-Host 'urlDecoded '  ([System.Web.HttpUtility]::UrlDecode($dataText))
	# $nvcoll =  [System.Web.HttpUtility]::ParseQueryString($dataText);
	# foreach($k in $nvcoll){
		# Write-Host 'Form field: ' $k ' : ' $nvcoll[$k]
	# }
	##OK
	
	foreach($k in $context.Request.QueryString){
		Write-Host 'QueryString field: ' $k ' : ' $context.Request.QueryString[$k]
	}
	
    $localPath = $requestUrl.LocalPath
	Write-Host "localPath $localPath" 
	
	#check if localpath starts with 'api'
	if($localPath.startsWith('/api')){
		$route = $routes.Get_Item('/api')
	}
	else{
		$route = $routes.Get_Item($localPath)
	}
	
  	Write-Host "route $route " 

    if ($route -eq $null)
    {	
		#if route not found, check if static file exists
		$fileFullName = $staticPath + '\' + $localPath
		$fileFullName = $fileFullName.Replace("/","\")
		$fileFullName = $fileFullName.Replace("\\","\")
		Write-Host "staticPath $staticPath" 
		Write-Host "fileFullName $fileFullName" 
		if(Test-Path $fileFullName){
			$isTogu = $false
			if([System.IO.Path]::GetExtension($fileFullName) -eq '.togu'){
				#static file with ".togu" extension
				$isTogu = $true
				$content = Get-Content $fileFullName | Out-String  #read file as text
				$content = "'" + $content + "'"
				$replacementlist = "<%#';#%>#;'"
				$content = replaceText $content $replacementlist
				$scriptBlock = [Scriptblock]::Create($content)  
				$content = toHTML $scriptBlock  ### OK				
			}
			else {
				#static file (.html, .jps, .png, .js, .css etc...)
				$content = [System.IO.File]::ReadAllBytes($fileFullName)
			}			
		}
        else{
			#if static file doesn't exist in $args[0] path, search it in common\web
			$fileFullName = "..\Common\web\" + $localPath
			if(Test-Path $fileFullName){
				$content = [System.IO.File]::ReadAllBytes($fileFullName)
			}			
			else{	
				$response.StatusCode = 404
				$content = [System.Text.Encoding]::UTF8.GetBytes("404 - File not found!")
			}
		}
    }
	else{
		 $content = & $route
	}
    if($content -ne $null)
    {       
	    if($isTogu){
        	$buffer = [System.Text.Encoding]::UTF8.GetBytes($content)
        }
		else{
			$buffer = $content
		}		
		$response.ContentLength64 = $buffer.Length
        $response.OutputStream.Write($buffer, 0, $buffer.Length)
    }    
    
    $responseStatus = $response.StatusCode
    Write-Host "$responseStatus"
	$response.Close()
}
