write-host 'initializing app'
[string]$source = Get-Content -Path ".\models\user_model.cs" | Out-String

#$Assem = [Reflection.Assembly]::LoadFrom("..\Common\lib\toguModels\bin\Release\toguModel.dll")
$Assem = [Reflection.Assembly]::LoadFrom("..\Common\lib\toguModels\bin\Debug\toguModel.dll")

Add-Type  -ReferencedAssemblies $Assem  -TypeDefinition $Source -Language CSharp  

$a = New-Object Togu.Models.SQLiteAdapter
$a.DataFileFullPath = "c:\temp\data\test2.sqlite"
$a.CreateDataFile()

[User]::SetAdapter($a)
