#deleteUser
function delete{
	param(
		$pars
	)	
	$userId = $pars.pathParams[0]
	
	#todo: use the "User" model to delete user and return userid or KO
	[User]::Delete_by_id($userId)
	
	$ret = @{}
	$ret['result'] = 'deleted user ' + $userId
	return toJson($ret)
}
