#editUser
function put{
	param(
		$pars
	)	
	
	$userid = $pars.pathParams[0]

	$username = $pars.formFields['username']
	$occupation = $pars.formFields['occupation']

	[User]::Update_by_id($userid, @{"Name" = $username; "Occupation" = $occupation})
	
	$ret = @{}
	$ret['result'] = "updated user " + $userid
	return toJson($ret)
}
