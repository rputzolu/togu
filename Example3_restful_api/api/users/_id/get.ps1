#getUser
function get{
	param(
		$pars
	)		
	$ret = @{}
	
	#use the User model to get user by id: $pars.pathParams[0]
	$u = [User]::Find_by_id($pars.pathParams[0])	
	
	$ret.id = $u.Id
	$ret.name = $u.Name
	$ret.occupation = $u.Occupation
		
	return toJson($ret) 
}