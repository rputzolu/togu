#listUsers
function get{
	param(
		$pars
	)		
	$ret = @{}
	
	#todo: use the User model to get a list of users
	#todo: filter using $pars.queryString
	
	
	$ret['all'] = @()
	# mock User.all
	# $ret.all += @{'id'='11'; 'name'='Renzo'; 'occupation'='software developer'}
	# $ret.all += @{'id'='12';'name'='Zorro'; 'occupation'='vendicatore'}
	# $ret.all += @{'id'='13';'name'='Zio Paperone'; 'occupation'='benestante'}
	
	foreach($u in [User]::All()){
		write-host $u.Id ' ' $u.Name ' ' $u.Occupation
		$ret.all += @{'id'=$u.Id; 'name'=$u.Name; 'occupation'=$u.Occupation}
	}
	

	
	return toJson($ret) 
}
