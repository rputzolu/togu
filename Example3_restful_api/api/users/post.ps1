#create a new user
function post{
	param(
		$pars
	)	
	$username = $pars.formFields['username']
	$occupation = $pars.formFields['occupation']
	
	#use the "User" model to create a new user and return userid or KO
	$justCreated = [User]::Create(@{"Name" = $username; "Occupation" = $occupation})
	
	$ret = @{}
	$ret['result'] = "created user " + $username
	return toJson($ret)
}
