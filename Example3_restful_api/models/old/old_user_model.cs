using System;
using System.Linq;
using System.Collections.Generic;


 namespace Togu.Models
 {
	public class User
	{
		private string name = "";
		private string occupation = "";
		private long id;
		
		public static List<User> All = new List<User>();
		
		public static User Find_by_id(long id2Find)
		{
			 User result = User.All.Find(
				delegate(User us)
				{
					return us.Id == id2Find;
				}
            );			
			return(result);						
		}
		
		public static User Delete_by_id(long id)
		{
			User result = User.Find_by_id(id);
			if(result != null){
				User.All.RemoveAll(x => x.Id == id);				
			}
			return result;
		}
		
		public static User Update_by_id(long id, string userName, string userOccupation)
		{
			User my_user = User.Find_by_id(id);
			if(my_user != null){
				my_user.Name = userName; 
				my_user.Occupation = userOccupation;
			}	
			return my_user;
		}
		
		
		//constructor
		public User(string userName, string userOccupation)
		{
			name = userName;
			occupation = userOccupation;			
		}
		
		//constructor
		public User()
		{		
		}
		
		public static User New()
		{
			User u  = new User();
			return u;
		}
		
		public static User New(string userName, string userOccupation)
		{
			User u  = new User(userName, userOccupation);		
			return u;
		}

		public static User Create(string userName, string userOccupation)
		{
			User u  = new User(userName, userOccupation);		
			u.Save();
			return u;
		}		

		public void Save()
		{
			id = 0;
			if(All.Count > 0){
				id = 1 + User.All.Max( obj => obj.Id );
			}
	
			Console.Write("id: " + id.ToString());
			//var maxObj = User.All.Where( obj => obj.Id == maxId );
			
			All.Add(this);
		}
			
		//properties
		public string Name
		{
			get 
			{
			   return name; 
			}
			set 
			{
			   name = value; 
			}
		}
		
		public string Occupation
		{
			get 
			{
			   return occupation; 
			}
			set 
			{	
			   occupation = value; 
			}
		}
		
		//read only 
		public long Id
		{
			get 
			{
			   return id; 
			}
		}
	}
	
	public class TestUser
	{
		public static void Main()
		{
			//User u = new User("renzo","meccanico");
			//User v = new User("pippo","ingegnere");
			
			User z = User.New();
			z.Name = "aaa";
			z.Save();
			
			User x = User.New();
			x.Name = "bbb";
			x.Save();
			
			List<User> lu = User.All;
			
			foreach (User o in lu)
			{
				Console.WriteLine(o.Id.ToString() + " " + o.Name + " " + o.Occupation);
			}			
		}		
	}
 }
