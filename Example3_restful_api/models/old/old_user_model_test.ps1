[string]$source = Get-Content -Path ".\user_model.cs" | Out-String
#write-host $source

Add-Type  -ReferencedAssemblies $Assem -TypeDefinition $Source -Language CSharp  
 
$u = [User]::New()
$u.Name = 'aaa'
$u.Occupation = 'bbb'
$u.Save()

$u = [User]::New()
$u.Name = 'qqqq'
$u.Occupation = 'pppp'
$u.Save()

$z = [User]::Create('pluto','cane')

foreach($u in [User]::All){
	write-host $u.Id ' ' $u.Name ' ' $u.Occupation
}
 
$f = [User]::Find_by_id(1)
$f.Name 

$f = [User]::Find_by_id(2)
$f.Name 

$f = [User]::Update_by_id(2, 'xxx', 'yyy')
$f.Name 

$ret = [User]::Delete_by_id(1)

foreach($u in [User]::All){
	write-host $u.Id ' ' $u.Name ' ' $u.Occupation
}
write-host $ret.Name