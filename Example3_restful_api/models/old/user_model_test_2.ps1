[string]$source = Get-Content -Path ".\user_model.cs" | Out-String
#write-host $source

$Assem = [Reflection.Assembly]::LoadFrom("C:\Renzo\Putzolu\Sandbox\Powershell\togu\Common\lib\toguModels\bin\Debug\toguModel.dll")
Add-Type  -ReferencedAssemblies $Assem  -TypeDefinition $Source -Language CSharp  

$a = New-Object Togu.Models.SQLiteAdapter
$a.DataFileFullPath = "c:\temp\data\test.sqlite"
[User]::SetAdapter($a)

$u = New-Object User 
$u.FirstName = 'aaa'
$u.LastName = 'ccc'
$u.Save()


$z = [User]::New()
$z.FirstName = 'pippo'
$z.Save()

$x = [User]::Create(@{"FirstName" = "Emy"; "Age" = 37})
Write-Host "Last inserted record Id: $($x.Id)" 

foreach($u in [User]::All()){
	write-host $u.Id ' ' $u.FirstName ' '  $u.LastName ' ' $u.Age
}
 
$f = [User]::Find_by_id($z.Id)
Write-Host "First Name: $($f.FirstName)" 


$f = [User]::Update_by_id($z.Id, @{"FirstName" = "Zorro"; "Age" = 30})
foreach($u in [User]::All()){
	write-host $u.Id ' ' $u.FirstName ' '  $u.LastName ' ' $u.Age
}

[User]::Delete_by_id($z.Id)

foreach($u in [User]::All()){
	write-host $u.Id ' ' $u.FirstName ' '  $u.LastName ' ' $u.Age
}
