#Todo: get paths from configuration file
function ajax2ps {
	param(
		$qStringParams = $null
	)	
	$qParams = fromJson $qStringParams 
	$response = @{}
	
	$response["greeting"]='Hi ' + $qParams["name"] + ', this is Powershell!';
		
	return toJson $response
}

<#TEST
cls
. ..\..\Common\lib\JsonSerialization.ps1
$response = ajax2ps('{"name":"Renzo"}');
Write-Output $response
#>
